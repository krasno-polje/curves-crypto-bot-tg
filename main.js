const x25519 = require('bcrypto/lib/x25519')
const base32 = require('hi-base32')

function bytesToBase32(bytes) {
    return base32.encode(bytes).replaceAll('=', '')
}

function base32ToHex(str) {
    const bytesArr = base32.decode.asBytes(str)
    return Buffer.from(bytesArr).toString('hex')
}

function base32ToBuffer(str) {
    return Buffer.from(base32.decode.asBytes(str))
}

function genKeyPair() {
    const prvKey = x25519.privateKeyGenerate()
    const pubKey = x25519.publicKeyCreate(prvKey)
    return {
        public: bytesToBase32(pubKey),
        private: bytesToBase32(prvKey)
    }
}

function deriveKey(prvKeyBase32, pubKeyBase32) {
    const prvKey = base32ToBuffer(prvKeyBase32)
    const pubKey = base32ToBuffer(pubKeyBase32)
    const sharedSecret = x25519.derive(pubKey, prvKey)
    return bytesToBase32(sharedSecret)
}

function isPrivateKeyBase32(str) {
    const bytes = base32ToBuffer(str)
    return x25519.privateKeyVerify(bytes)
}

function isPublicKeyBase32(str) {
    const bytes = base32ToBuffer(str)
    return x25519.publicKeyVerify(bytes)
}

// Here is some stuff to test the bcrypto library. Don't pay attention
function main() {
    const prvKey = x25519.privateKeyGenerate()
    const prvKeyExported = x25519.privateKeyExport(prvKey)

    const pubKey = x25519.publicKeyCreate(prvKey)

    console.log(`Private key hex:\n${prvKey.toString('hex')}\n`)
    //console.log(`Private key:\n${JSON.stringify(prvKeyExported, null, 2)}`)

    console.log(`Public key hex:\n${pubKey.toString('hex')}\n`)

    // prv 18470d9ab47ac68c7a1d752f192a48c5b238d13025d5a268d7490a36c807d375
    // pub 49a79e46f85892e818f229b8341a404d2f56a39cca3559080d17c359fd17ba6f

    const bobPrvKeyHex = '18470d9ab47ac68c7a1d752f192a48c5b238d13025d5a268d7490a36c807d375'
    const bobPrvKey = Buffer.from(bobPrvKeyHex, 'hex')
    const bobPubKey = x25519.publicKeyCreate(bobPrvKey)

    console.log(`Bob's public key:\n${bobPubKey.toString('hex')}\n`)
    console.log(`Bob's private key\n${bobPrvKey.toString('hex')}\n`)

    const prvKeyBase32 = 'DBDQ3GVUPLDIY6Q5OUXRSKSIYWZDRUJQEXK2E2GXJEFDNSAH2N2Q'
    console.log(`Bob's private key hex:\n${base32ToHex(prvKeyBase32)}\n`)
}

module.exports = {
    genKeyPair, 
    deriveKey, 
    isPrivateKeyBase32, 
    isPublicKeyBase32, 
    base32ToBuffer
}
