process.env["NTBA_FIX_319"] = 1 // for supression of a useless warning
require('dotenv').config()
const TelegramBot = require('node-telegram-bot-api')

const { 
    genKeyPair, 
    deriveKey, 
    isPrivateKeyBase32, 
    isPublicKeyBase32,
    base32ToBuffer
} = require('./main.js')

const COMMAND_START = '/'

// now it does not mean anything
const CommandType = {
    PROC: 'PROC',
    FULL_FUNC: 'FULL_FUNC'
}

function toJSON(obj) {
    return JSON.stringify(obj, null, 2)
}

function onCommandGen(msg) {
    const keyPair = genKeyPair()

    const messageToSendText = `You have generated a new keypair\\! 🎉
Now you can use it for Diffie–Hellman key exchange and for whatever you want\\!

*Public key:*
\`${keyPair.public}\`

*Private key:*
\`${keyPair.private}\``

    bot.sendMessage(msg.from.id, messageToSendText, {parse_mode: 'MarkdownV2'})
        .then(res => {
            //console.log(res)
        })
}

function isBase32(str) {
    const regex = /^[A-Z2-7]+=*$/
    return regex.exec(str)
}

function onCommandDerive(msg) {
    const args = msg.text.split(' ').slice(1)
    if (args.length != 2) {
        bot.sendMessage(msg.from.id, 'Please, enter the two arguments: private key and public key')
            .then(res => {
                //console.log('Msg successfully sent')
            })
        return
    }
    const prvKeyBase32 = args[0]
    const pubKeyBase32 = args[1]

    if (!isBase32(prvKeyBase32)) {
        bot.sendMessage(msg.from.id, 'The private key is not in base32 format')
            .then()
        return
    }
    if (!isPrivateKeyBase32(prvKeyBase32)) {
        bot.sendMessage(msg.from.id, 'The first argument must be Curve25519 private key')
            .then()
        return
    }

    if (!isBase32(pubKeyBase32)) {
        bot.sendMessage(msg.from.id, 'The public key is not in base32 format')
            .then()
        return
    }
    if (!isPublicKeyBase32(pubKeyBase32)) {
        bot.sendMessage(msg.from.id, 'The second argument must be Curve25519 public key')
            .then()
        return
    }

    const derivedKeyBase32 = deriveKey(prvKeyBase32, pubKeyBase32)
    const messageToSend = `*Derived key:*
\`${derivedKeyBase32}\``
    bot.sendMessage(msg.from.id, messageToSend, {parse_mode: 'MarkdownV2'})
        .then(res => {
            console.log('Msg successfully sent')
        })
}

function onCommandStart(msg) {
    const messageToSendText = `Greetings\\! This bot allows you to play with elliptic curves various ways\\. Now only Curve25519 is supported, but other ones support will be added later\\.\n
*List of commands:*
/gen \\- generates keypair in Base32 encoding\\. Other encodings support will be added later\\.
/derive \\<alice\\-private\\-key\\> \\<bob\\-public\\-key\\> \\- derives a key \\(shared secret\\) for a private key of one keypair and for a public key of another one\\.`
    bot.sendMessage(msg.from.id, messageToSendText, {parse_mode: 'MarkdownV2'})
        .then()
}

function onCommandConvertToHex(msg) {
    const strToConvert = msg.text.split(' ')[1]
    if (strToConvert == undefined) {
        const messageTextToSend = `Please, provide a Base32-encoded string`
        bot.sendMessage(msg.from.id, messageTextToSend)
            .then()
        return
    }
    if (!isBase32(strToConvert)) {
        const messageTextToSend = `The argument must be Base32-encoded string`
        bot.sendMessage(msg.from.id, messageTextToSend)
            .then()
        return
    }
    const strToConvertBytes = base32ToBuffer(strToConvert)
    const resultHex = strToConvertBytes.toString('hex')
    const messageTextToSend = `*Your hex\\-encoded string:*\n\`${resultHex}\``
    bot.sendMessage(msg.from.id, messageTextToSend, {parse_mode: 'MarkdownV2'})
        .then()
}

const commands = [
    {
        name: 'start',
        type: CommandType.PROC,
        callback: onCommandStart
    },
    {
        name: 'gen',
        type: CommandType.PROC,
        callback: onCommandGen
    },
    {
        name: 'derive',
        type: CommandType.FULL_FUNC,
        callback: onCommandDerive
    },
    {
        name: 'convert_to_hex',
        type: CommandType.FULL_FUNC,
        callback: onCommandConvertToHex
    }
]

function handleCommand(msg) {
    const cmdName = msg.text.split(' ')[0].substring(1)
    const cmd = commands.find(command => command.name == cmdName)
    if (cmd == undefined)
        return
    console.log(`${msg.from.username} issued command ${cmdName}`)
    cmd.callback(msg)
}

if (process.env.TG_BOT_API_TOKEN == undefined) {
    console.log(
        `Please, specify your Telegram bot API-token in the environment variable TG_BOT_API_TOKEN`)
    process.exit(1)
}
const botApiToken = process.env.TG_BOT_API_TOKEN
const bot = new TelegramBot(botApiToken, { polling: true })

bot.on('message', (msg) => {
    if (msg.text.startsWith(COMMAND_START)) {
        handleCommand(msg)
    }
})

bot.on('polling_error', (error) => {
    console.log(error)
})

console.log(`🤖 The bot has started 🤖`)
