# Usage
Fetch the required packages: 

```bash
npm install
```

Put your bot api token in the environment variable:

```bash
echo "TG_BOT_API_TOKEN='<your-bot-api-token>'" > .env
```

Run the the bot:

```bash
npm start
```
